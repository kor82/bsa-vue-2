import Vue from 'vue';

// local state
const state = {
  albums: [
  {
    'userId': 1,
    'id': 1,
    'title': 'quidem molestiae enim',
    'url': 'https://via.placeholder.com/150/392537'
  },
  {
    'userId': 1,
    'id': 2,
    'title': 'sunt qui excepturi placeat culpa',
    'url': 'https://via.placeholder.com/150/602b9e'
  },
  {
    'userId': 2,
    'id': 3,
    'title': 'omnis laborum odio',
    'url': 'https://via.placeholder.com/150/372c93'
  },
  {
    'userId': 2,
    'id': 4,
    'title': 'non esse culpa molestiae omnis sed optio',
    'url': 'https://via.placeholder.com/150/a7c272'
  },
  {
    'userId': 2,
    'id': 5,
    'title': 'eaque aut omnis a',
    'url': 'https://via.placeholder.com/150/c70a4d'
  },
  {
    'userId': 3,
    'id': 6,
    'title': 'natus impedit quibusdam illo est',
    'url': 'https://via.placeholder.com/150/501fe1'
  },
  {
    'userId': 3,
    'id': 7,
    'title': 'quibusdam autem aliquid et et quia',
    'url': 'https://via.placeholder.com/150/35185e'
  },
  {
    'userId': 3,
    'id': 8,
    'title': 'qui fuga est a eum',
    'url': 'https://via.placeholder.com/150/c96cad'
  },
  {
    'userId': 4,
    'id': 9,
    'title': 'saepe unde necessitatibus rem',
    'url': 'https://via.placeholder.com/150/4d564d'
  },
  {
    'userId': 4,
    'id': 10,
    'title': 'distinctio laborum qui',
    'url': 'https://via.placeholder.com/150/ea51da'
  },
  {
    'userId': 4,
    'id': 11,
    'title': 'quam nostrum impedit mollitia quod et dolor',
    'url': 'https://via.placeholder.com/150/4f5b8d'
  },     
  ]
};

const getters = {
  // state is module's local state
  getAlbumById: (state) => {
    return id => state.albums.filter(album => album.id == id)[0]
  },

  filterByUser: (state) => {
    return userId => state.albums.filter(album => album.userId == userId);
  }
};

const mutations = {
  ADD_ALBUM(state, album) {
    let lastId = Math.max(...state.albums.map(album => album.id)) + 1;
    console.log('album');
    console.log(album);
    state.albums.push({
      id: lastId,
      userId: album.userId,
      title: album.title,
      url: album.url
    });
  },

  DELETE_ALBUM(state, albumId) {
    const ind = state.albums.findIndex(album => album.id === albumId);

    if (ind !== -1) {
      state.albums.splice(ind, 1);
    }

    state.photos = state.photos.filter(photo => photo.albumId !== albumId);
  },

  EDIT_ALBUM(state, { albumId, data }) {
    const ind = state.albums.findIndex(album => album.id === albumId);

    if (ind !== -1) {
      const updatedAlbum = {
        id: albumId,
        title: data.title,
        url: data.url
      };

      Vue.set(state.albums, ind, updateAlbum);
    }
  }
};

const actions = {
  // first param is context object
  addAlbum({ state, commit, rootState }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('ADD_ALBUM', data);
        resolve();
      }, 250);
    });
  },

  deleteAlbum({ commit }, albumId) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('DELETE_ALBUM', albumId);
        resolve();
      }, 250);
    });
  },

  editAlbum({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('EDIT_ALBUM', data);
        resolve();
      }, 250);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};