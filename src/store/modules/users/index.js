import Vue from 'vue';

// local state
const state = {
  users: [
    {
      id: 1,
      name: 'Jimm',
      email: 'jimm@example.com',
      avatar: 'https://i.pravatar.cc/50?u=jimm@example.com',
      show: true
    },
    {
      id: 2,
      name: 'Bob',
      email: 'bob@example.com',
      avatar: 'https://i.pravatar.cc/50?u=bob@example.com',
      show: true
    },
    {
      id: 3,
      name: 'Ann',
      email: 'ann@example.com',
      avatar: 'https://i.pravatar.cc/50?u=ann@example.com',
      show: true
    },    
    {
      id: 4,
      name: 'John',
      email: 'john@example.com',
      avatar: 'https://i.pravatar.cc/50?u=john@example.com',
      show: true
    },      
  ]
};

const getters = {
  // state is module's local state
  getUserById: (state) => {
    return id => state.users.filter(user => user.id == id)[0]
  },

  filterByField: (state) => {
    return (field, input) => state.users.filter(user => {
      console.log(user[field].toLowerCase().indexOf(input.toLowerCase()) > -1);
        return user[field].toLowerCase().indexOf(input.toLowerCase()) > -1
    })
  }
};

const mutations = {
  ADD_USER(state, user) {
    let lastId = Math.max(...state.users.map(user => user.id)) + 1;

    state.users.push({
      id: lastId,
      name: user.name,
      email: user.email,
      avatar: 'https://i.pravatar.cc/50?u='+user.name
    });
  },

  DELETE_USER(state, userId) {
    const ind = state.users.findIndex(user => user.id === userId);

    if (ind !== -1) {
      state.users.splice(ind, 1);
    }
  },

  EDIT_USER(state, { userId, data }) {
    const ind = state.users.findIndex(user => user.id === userId);

    if (ind !== -1) {
      const updatedUser = {
        id: userId,
        name: data.name,
        email: data.email
      };

      Vue.set(state.users, ind, updatedUser);
    }
  }
};

const actions = {
  // first param is context object
  addUser({ state, commit, rootState }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('ADD_USER', data);
        resolve();
      }, 250);
    });
  },

  deleteUser({ commit }, userId) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('DELETE_USER', userId);
        resolve();
      }, 250);
    });
  },

  editUser({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('EDIT_USER', data);
        resolve();
      }, 250);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};