import Vue from 'vue';

// local state
const state = {
  photos: [
  {
    'albumId': 1,
    'id': 1,
    'title': 'accusamus beatae ad facilis cum similique qui sunt',
    'url': 'https://via.placeholder.com/600/92c952',
  },
  {
    'albumId': 1,
    'id': 2,
    'title': 'reprehenderit est deserunt velit ipsam',
    'url': 'https://via.placeholder.com/600/771796',
  },
  {
    'albumId': 2,
    'id': 3,
    'title': 'officia porro iure quia iusto qui ipsa ut modi',
    'url': 'https://via.placeholder.com/600/24f355',
  },
  {
    'albumId': 2,
    'id': 4,
    'title': 'culpa odio esse rerum omnis laboriosam voluptate repudiandae',
    'url': 'https://via.placeholder.com/600/d32776',
  },
  {
    'albumId': 3,
    'id': 5,
    'title': 'natus nisi omnis corporis facere molestiae rerum in',
    'url': 'https://via.placeholder.com/600/f66b97',
  },
  {
    'albumId': 3,
    'id': 6,
    'title': 'reprehenderit est deserunt velit ipsam',
    'url': 'https://via.placeholder.com/600/771796',
  },
  {
    'albumId': 4,
    'id': 7,
    'title': 'officia porro iure quia iusto qui ipsa ut modi',
    'url': 'https://via.placeholder.com/600/2342332',
  },
  {
    'albumId': 4,
    'id': 8,
    'title': 'culpa odio esse rerum omnis laboriosam voluptate repudiandae',
    'url': 'https://via.placeholder.com/600/54655g',
  },
  {
    'albumId': 4,
    'id': 9,
    'title': 'natus nisi omnis corporis facere molestiae rerum in',
    'url': 'https://via.placeholder.com/600/6785434f',
  },  
  ]
};

const getters = {
  // state is module's local state
  filterByAlbum: (state) => {
    return albumId => state.photos.filter(photo => photo.albumId == albumId);
  }    
};

const mutations = {
  ADD_PHOTO(state, photo) {
    let lastId = Math.max(...state.photos.map(photo => photo.id)) + 1;

    state.photos.push({
      id: lastId,
      albumId: photo.albumId,
      title: photo.title,
      url: photo.url
    });
  },

  DELETE_PHOTO(state, photoId) {
    const ind = state.photos.findIndex(photo => photo.id === photoId);

    if (ind !== -1) {
      state.photos.splice(ind, 1);
    }
  },
};

const actions = {
  // first param is context object
  addPhoto({ state, commit, rootState }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('ADD_PHOTO', data);
        resolve();
      }, 250);
    });
  },

  deletePhoto({ commit }, photoId) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('DELETE_PHOTO', photoId);
        resolve();
      }, 250);
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};