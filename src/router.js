import Vue from 'vue'
import Router from 'vue-router'
import UserList from './components/UserList.vue'
import AddUser from './components/AddUser.vue'
import UserItem from './components/UserItem.vue'
import EditUser from './components/EditUser.vue'
import AlbumList from './components/AlbumList.vue'
import EditAlbum from './components/EditAlbum.vue'
import AddAlbum from './components/AddAlbum.vue'
import AlbumItem from './components/AlbumItem.vue'
import PhotoList from './components/PhotoList.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: "/users"
    },
    {
      path: '/users',
      name: 'users',
      component: UserList
    },
    {
      path: '/users/add',
      name: 'add-user',
      component: AddUser
    },
    {
      path: '/users/:id',
      name: 'show-user',
      component: UserItem
    },
    {
      path: '/users/:id/edit',
      name: 'edit-user',
      component: EditUser
    },         
    {
      path: '/albums',
      name: 'albums',
      component: AlbumList,
    },     
    {
      path: '/albums/:id',
      name: 'show-album',
      component: AlbumItem,
    },
    {
      path: '/albums/:id/edit',
      name: 'edit-album',
      component: EditAlbum
    } 
  ]
})
